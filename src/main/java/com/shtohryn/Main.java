package com.shtohryn;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Array array = new Array();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Task A");
        System.out.println("Enter the length of random 1st array");
        int length = scanner.nextInt();
        int[] array1 = array.createArray(length);
        System.out.println("Enter the length of random 2nd array");
        length = scanner.nextInt();
        int[] array2 = array.createArray(length);
        System.out.println("First random array");
        array.printArray(array1);
        System.out.println("Second random array");
        array.printArray(array2);
        System.out.println("Result array with equals elements from array1 and array2");
        int[] res = array.createArrayWithElementsFrom2Arrays(array1, array2);
        array.printArray(res);
        System.out.println("without elements repeated more than two times");
        System.out.print(Arrays.toString(array.killingReapitingElements(res)));
        System.out.println("\n**************************************************** ");
        System.out.print(Arrays.toString(array.withOutReapittingElements(res)));

    }
}
