package com.shtohryn;
/**
 * Class Array with methods:
 * printArray()
 * createArrayWithElementsFrom2Arrays
 */

import java.util.Arrays;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class Array {
    /**
     * @param length- size of random array
     * @return res []- array with random elements
     */
    public int[] createArray(int length) {
        Random random = new Random();
        int[] res = new int[length];
        for (int i = 0; i < res.length; i++) {
            res[i] = 1 + random.nextInt(10);
        }
        return res;
    }

    /**
     * method for printing array
     *
     * @param array
     */
    public void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }

    /**
     * Method merging array1 and array2 with equals elements
     *
     * @param array1 first random array
     * @param array2 second random array
     * @return res []- result  array fielded with equals elements from array1 and array2
     */
    public int[] createArrayWithElementsFrom2Arrays(int[] array1, int[] array2) {
        int[] res = new int[array1.length > array2.length ? array1.length : array2.length];
        int length1 = array1.length > array2.length ? array1.length : array2.length;
        int length2 = array1.length < array2.length ? array1.length : array2.length;
        int[] arr1 = new int[length1];
        int[] arr2 = new int[length2];
        if (array1.length > array2.length) {
            System.arraycopy(array1, 0, arr1, 0, (array1.length));
            System.arraycopy(array2, 0, arr2, 0, (array2.length));
        } else {
            System.arraycopy(array2, 0, arr1, 0, (array2.length));
            System.arraycopy(array1, 0, arr2, 0, (array1.length));
        }
        int iter = 0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr2[j] == arr1[i] && (iter < arr1.length - 1)) {
                    res[j] = arr2[j];
                    iter++;
                }
            }
        }
        // loop removing null elements
        for (int i = 0; i < res.length; i++) {
            int j = 0;
            if (res[i] == 0 && res[i] != 0)
                j++;
            if (res[i] == 0 && j <= 1)
                res[i] = res[0];
        }
        return res;
    }

    int[] killingReapitingElements(int array[]) {
        int n = array.length;

        for (int i = 0, m = 0; i != n; i++, n = m) {
            for (int j = m = i + 1; j != n; j++) {
                if (array[j] != array[i]) {
                    if (m != j) {
                        array[m] = array[j];
                    }
                    m++;
                }
            }
        }

        int[] result = new int[n];
        if (n != array.length) {
            for (int i = 0; i < n; i++) {
                result[i] = array[i];
            }
            return result;
        } else {
            return array;
        }
    }

    public int[] withOutReapittingElements(int array[]) {
        int count = 0;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1]) {
                continue;
            } else {
                count++;
            }
        }

        int result[] = new int[count];
        int pos = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) {
                continue;
            } else {
                result[pos] = array[i - 1];
                pos++;
            }
        }

        return result;
    }
}
